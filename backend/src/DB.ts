import * as sqlite3 from 'sqlite3';

let connection: DB | null = null;

class DB
{
  db: sqlite3.Database;

  constructor() {
//    this.db = new sqlite3.Database('./db.sqlite', (err) => {
    this.db = new sqlite3.Database(':memory:', (err) => {
      if (err) {
        console.error(err.message);
      }
      console.log('Database Connected !');
    });
    connection = this;
  }

  static Get() {
    if(connection == null) throw "Access to non connected DB";
    return connection;
  }

  async close() {
    DB.Get().db.close();
  }

  async Init() {
    let version: number = -1;

    try {
      version = await this.All('SELECT num FROM version', []);
    }catch(ex){}
    console.log(`Version ${version}`);

    if(version < 0) {
      console.log(`Create fresh DB`);
      await this.Run('CREATE TABLE version(num INTEGER)', []);
      await this.Run('INSERT INTO version(num) VALUES(?)', [1]);
      console.log(await this.All('SELECT * FROM version', []));

      await this.Run(`CREATE TABLE user(num INTEGER PRIMARY KEY,
                                        name text NOT NULL,
                                        pass text NOT NULL
                                         )`, []);
      await this.Run('INSERT INTO user(num, name, pass) VALUES(?, ?, ?)', [1, 'admin', '1234']);
      await this.Run('INSERT INTO user(num, name, pass) VALUES(?, ?, ?)', [2, 'user', '5678']);
      console.log(await this.All('SELECT * FROM user', []));
    }else{
      console.log(`DB Version is: ${version}`);
    }
  }

  async Run(query: string, param: any[]) : Promise<any>{
    return new Promise<any>((resolve: any, reject: any) => {
      this.db.run(query, param, (err: any) => {
        if(err) reject(err);
        resolve();
      });
    });
  }

  async All(query: string, param: any[]) : Promise<any>{
    return new Promise<any>((resolve: any, reject: any) => {
      this.db.all(query, param, (err: any, rows: any) => {
        if(err) reject(err);
        resolve(rows);
      });
    });
  }

  async Get(query: string, param: any[]) : Promise<any>{
    return new Promise<any>((resolve: any, reject: any) => {
      this.db.get(query, param, (err: any, rows: any) => {
        if(err) reject(err);
        resolve(rows);
      });
    });
  }
}

export default DB;
