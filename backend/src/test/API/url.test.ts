import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import assert from 'assert';
import LoginAs, { AuthToken, LOGINDATA } from "./login";



describe('URL Routing', () => {
  it('/test', (done) => {
    axios.get(`/api/test`).then(res => {
      assert.deepEqual(res.data, {test: 'Hello!'});
      done();
    }).catch(done);
  });

  it('/test/a', (done) => {
    axios.get(`/api/test/213`).then(res => {
      assert.deepEqual(res.data, {url: '/test/a', a: 213});
      done();
    }).catch(done);
  });

  it('/test/a/b', (done) => {
    axios.get(`/api/test/213/578`).then(res => {
      assert.deepEqual(res.data, {url: '/test/a/b', a: 213, b: 578});
      done();
    }).catch(done);
  });

  it('/test/a/word', (done) => {
    axios.get(`/api/test/213/hi`).then(res => {
      assert.deepEqual(res.data, {url: '/test/a/word', a: 213, word: 'hi'});
      done();
    }).catch(done);
  });

});
