import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

const LOGINDATA = {
  admin: {user: 'admin', pass:'1234'}
};


let AuthToken = "";
function LoginAs(udata: {user: string, pass: string}) {
  return new Promise<AxiosResponse<any>>((resolve, rej) => {
    axios.post('/login', udata)
        .then((res) => {
      AuthToken = res.data.token;
      resolve(res);
    }).catch(rej);
  });
}

export {LOGINDATA, AuthToken};
export default LoginAs;
