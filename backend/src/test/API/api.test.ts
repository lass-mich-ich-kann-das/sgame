import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import assert from 'assert';
import LoginAs, { AuthToken, LOGINDATA } from "./login";



describe('API', () => {

  it('/planet/my', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {

      axios.get('/api/planet/my', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        done();
      }).catch(done);
    });
  });

  it('/galaxy/0/0', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {

      axios.get('/api/galaxy/0/0', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        assert.deepEqual(res.data, []);
        done();
      }).catch(done);
    });
  });

  it('/galaxy/0/1', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {

      axios.get('/api/galaxy/0/1', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        assert.equal(res.data, []);
        done();
      }).catch(done);
    });
  });

  it('/galaxy/1/2', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {

      axios.get('/api/galaxy/1/2', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        assert.equal(res.data, []);
        done();
      }).catch(done);
    });
  });

  it('/planet/building/up/0/1/0/MineMetal', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {

      axios.get('/api/planet/building/up/0/1/0/MineMetal', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        assert.equal(res.data, []);
        done();
      }).catch(done);
    });
  });

  it('/fleet/list', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {
      axios.get('/api/fleet/list', {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        assert.equal(res.data, []);
        done();
      }).catch(done);
    });
  });

  it('/fleet/attack/0/1/0/0/2/3', (done) => {
    LoginAs(LOGINDATA.admin).then(res => {
      axios.post('/api/fleet/attack/0/1/0/0/2/3', {
        Fighter: 2
      }, {
        headers: { Authorization: `Bearer ${AuthToken}` }
      }).then((res: AxiosResponse<any>) => {
        console.debug(res.data);
        assert.equal(res.data, []);
        done();
      }).catch(done);
    });
  });



});
