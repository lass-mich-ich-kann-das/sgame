import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import assert from 'assert';
import Planet from "../../Game/Planet";
import Position from "../../Game/Position";
import Ressources, { ResTypes } from "../../Game/Ressources";
import Buildings from "../../Game/Buildings";
import BUILDINGS from "../../Game/Buildings/BuildingList";


describe('Building Outputs', () => {
  it('MineMetal', () => {
    let planet: Planet = new Planet(new Position(0,0,0), 0, "Planet", new Ressources(), new Buildings());
    let production: Array<Ressources> = [];
    [0,1,2,3,4,5,6,7,8,9,10].forEach(offset => production.push(BUILDINGS.MineMetal.Generate(planet, offset)));
    production.forEach((p, idx) => {
      let row: string = `${idx}:\tMineMetal`;
      for(let [key, value] of Object.entries(p.JSON())) {
        row += `\t${key}: ${(<number>value).toFixed(2)},`;
      };
      console.log(row);
    });
  });

  it('MineCrystal', () => {
    let planet: Planet = new Planet(new Position(0,0,0), 0, "Planet", new Ressources(), new Buildings());
    let production: Array<Ressources> = [];
    [0,1,2,3,4,5,6,7,8,9,10].forEach(offset => production.push(BUILDINGS.MineCrystal.Generate(planet, offset)));
    production.forEach((p, idx) => {
      let row: string = `${idx}:\tMineCrystal`;
      for(let [key, value] of Object.entries(p.JSON())) {
        row += `\t${key}: ${(<number>value).toFixed(2)},`;
      };
      console.log(row);
    });
  });

  it('PowerPlant', () => {
    let planet: Planet = new Planet(new Position(0,0,0), 0, "Planet", new Ressources(), new Buildings());
    let production: Array<Ressources> = [];
    [0,1,2,3,4,5,6,7,8,9,10].forEach(offset => production.push(BUILDINGS.PowerPlant.Generate(planet, offset)));
    production.forEach((p, idx) => {
      let row: string = `${idx}:\tPowerPlant`;
      for(let [key, value] of Object.entries(p.JSON())) {
        row += `\t${key}: ${(<number>value).toFixed(2)},`;
      };
      console.log(row);
    });
  });
});
