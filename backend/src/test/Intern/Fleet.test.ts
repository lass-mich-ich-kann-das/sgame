import assert from 'assert';
import Planet from '../../Game/Planet';
import Position from '../../Game/Position';
import { EShips } from '../../Game/Ships/ShipList';
import Fleet from '../../Game/Fleet';
import { ResTypes } from '../../Game/Ressources';
import EBuildingType from '../../Game/Buildings/EBuildingType';


describe('Fleet', () => {
  let attackerPlanet = new Planet(new Position(0,0,1), 1, "Colony 0:0:1");
  attackerPlanet.fleet.Append({
    [EShips.Fighter]: 10,
    [EShips.SolarSat]: 20
  });

  let defenderPlanet = new Planet(new Position(0,0,2), 1, "Colony 0:0:1");
  defenderPlanet.fleet.Append({
    [EShips.Fighter]: 123
  });
  
  it('API Output', () => {
    assert.deepEqual(attackerPlanet.fleet.JSON(), {
      direction: 0,
      timeOfArrival: -1,
      home: attackerPlanet.position.JSON(),
      target: undefined,
      ships: {
        [EShips.Fighter]: 10,
        [EShips.SolarSat]: 20
      }});

    assert.deepEqual(defenderPlanet.fleet.JSON(), {
      direction: 0,
      home: defenderPlanet.position.JSON(),
      timeOfArrival: -1,
      target: undefined,
      ships: {
        [EShips.Fighter]: 123,
        [EShips.SolarSat]: 0
      }});
    });

  it('Build without Res & Prereqs', () => {
    let prevFighter = attackerPlanet.fleet.ships[EShips.Fighter];
    let result = attackerPlanet.BuildShips({
      [EShips.Fighter]: 2
    });

    assert.deepEqual(result, {
      error: [
        {
          message: 'Prereqs for Fighter not met at building Shipyard   is 0 should 2'
        }
      ]
    });

    assert.equal(prevFighter, attackerPlanet.fleet.ships[EShips.Fighter]);
  });

  it('Build without Res', () => {
    attackerPlanet.buildings.buildingsLvl[<string>EBuildingType.Shipyard] = 2;

    let result = attackerPlanet.BuildShips({
      [EShips.Fighter]: 2
    });


    assert.deepEqual(result, {
      error: [
        {
          message: 'Not enugh Ressources\n' +
            '      Need: {"ressources":{"energyGenerated":0,"energyConsumed":0,"metal":60,"crystal":20,"deuterium":0}}\n' +
            '      have: {"ressources":{"energyGenerated":0,"energyConsumed":0,"metal":0,"crystal":0,"deuterium":0}}'
        }
      ]
    });
  });

  it('Build', () => {
    let prevFighter = attackerPlanet.fleet.ships[EShips.Fighter];
    let resStart = {
      metal: attackerPlanet.ressources.Get(ResTypes.metal),
      crystal: attackerPlanet.ressources.Get(ResTypes.crystal)
    };

    console.log(attackerPlanet.ressources);
    attackerPlanet.ressources.Add(ResTypes.metal, 2 * 30);
    attackerPlanet.ressources.Add(ResTypes.crystal, 2 * 10);
    console.log(attackerPlanet.ressources);

    let result = attackerPlanet.BuildShips({
      [EShips.Fighter]: 2
    });

    assert.deepEqual(result, {    });

    assert.equal(attackerPlanet.ressources.Get(ResTypes.metal), resStart.metal);
    assert.equal(attackerPlanet.ressources.Get(ResTypes.crystal), resStart.crystal);
    assert.equal(prevFighter + 2, attackerPlanet.fleet.ships[EShips.Fighter]);
  });



  it('Fight', () => {
    assert.ok(attackerPlanet.fleet.Fight(defenderPlanet.fleet));
  });


  it('FormFleet & Join', () => {
    let prevFighterOnPlanet = attackerPlanet.fleet.JSON().ships.Fighter;
    let fleet = attackerPlanet.fleet.FormFleet({
      [EShips.Fighter]: 2
    });

    assert.ok(fleet);
    console.debug({newFleet: fleet.JSON()});
    console.debug({planetFleet: attackerPlanet.fleet.JSON()});

    assert.deepEqual(fleet.JSON(), {
      direction: 0,
      home: attackerPlanet.position.JSON(),
      timeOfArrival: -1,
      target: undefined,
      ships: {
        [EShips.Fighter]: 2,
        [EShips.SolarSat]: 0
      }});

    assert.deepEqual(attackerPlanet.fleet.JSON(), {
      direction: 0,
      home: attackerPlanet.position.JSON(),
      timeOfArrival: -1,
      target: undefined,
      ships: {
        [EShips.Fighter]: prevFighterOnPlanet -2,
        [EShips.SolarSat]: 20
      }});
  
    attackerPlanet.fleet.Join(fleet);

    assert.equal(prevFighterOnPlanet, attackerPlanet.fleet.JSON().ships.Fighter);
    assert.deepEqual(attackerPlanet.fleet.JSON(), {
      direction: 0,
      home: attackerPlanet.position.JSON(),
      timeOfArrival: -1,
      target: undefined,
      ships: {
        [EShips.Fighter]: prevFighterOnPlanet,
        [EShips.SolarSat]: 20
      }});
  });
});
