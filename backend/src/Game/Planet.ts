import Buildings from './Buildings';
import Position from './Position';
import Ressources from './Ressources';
import Fleet from './Fleet';
import * as DEBUG from 'debug';
import ShipTypes from './Ships/ShipList';

const LOG = DEBUG.default('Planet');
const LOGV = LOG.extend('Verbose');


let PLANETS: Array<Array<Array<Planet>>> = [[]];

class Planet {
  name: string;
  playerId: number;
  ressources: Ressources;
  resGeneration: Ressources | null;
  buildings: Buildings;
  position: Position;
  fleet: Fleet;

  constructor(position: Position,
              playerId: number, 
              name: string, 
              ressources: Ressources = new Ressources({}),
              buildings: Buildings = new Buildings({}),
              fleet: Fleet | null = null) {
    this.position = position;
    this.name = name;
    this.playerId = playerId;
    this.ressources = ressources;
    this.buildings = buildings;
    this.buildings.SetPlanet(this);
    this.resGeneration = null;
    this.fleet = fleet || new Fleet(this);

    LOG(`Created Planet. User:${playerId}   Pos: ${JSON.stringify(this.position.JSON())}`);

    if(!PLANETS[this.position.galaxy]) PLANETS[this.position.galaxy] = [];
    if(!PLANETS[this.position.galaxy][this.position.solar]) PLANETS[this.position.galaxy][this.position.solar] = [];
    PLANETS[this.position.galaxy][this.position.solar][this.position.idx] = this;
  }

  jsonShort() {
    return {
      position: this.position.JSON(),
      playerId: this.playerId,
      name: this.name
    };
  }

  JSON(): any {
    return {
      position: this.position.JSON(),
      playerId: this.playerId,
      name: this.name,
      ressources: this.ressources.JSON(),
      buildings: this.buildings.Stats(),
      resGeneration: this.resGeneration?.JSON()
    };
  }

  Update() {
    Buildings.UpdateBuildings(this);
  }

  BuildShips(ships: {[param: string]: number}): {error?: Array<{message: string}>} {
    LOGV(`Building Ships `);
    let errors: Array<{message: string}> = [];

    Fleet.GetTypes().forEach(st => {
      if(typeof ships[st] === 'number'){
        let preReqs = ShipTypes[st].UpgradeReqs(0).buildings;
        if(preReqs){
          Object.entries(preReqs).forEach(([req, level]) => {
            let blvl = this.buildings.GetLevel(req);
            if(blvl < level){
              errors.push({message: `Prereqs for ${st} not met at building ${req}   is ${blvl} should ${level}`});
            }
          });
        }
      }
    });
    if(errors.length){
      return {error: errors};
    }

    let totalRessources = new Ressources();
    Fleet.GetTypes().forEach(st => {
      if(typeof ships[st] === 'number'){
        let resForThisType = ShipTypes[st].UpgradeReqs(0).ressources;
        if(resForThisType){
          let res = new Ressources(resForThisType);
          res.Multiply(ships[st]);
          totalRessources.AddRes(res);
        }
      }
    });

    if(!this.ressources.Atleast(totalRessources)) {
      LOGV(`Requiring: ${JSON.stringify(totalRessources)}`);
      LOGV(`Have: ${JSON.stringify(this.ressources)}`);
      errors.push({message: `Not enugh Ressources
      Need: ${JSON.stringify(totalRessources)}
      have: ${JSON.stringify(this.ressources)}`});
      return {error: errors};
    }

    this.ressources.Remove(totalRessources);
    this.fleet.Append(ships);  
    return {};
  }

  GetPosShort(): string {
    let pos = this.position;
    return `${pos.galaxy}:${pos.solar}:${pos.idx}`;
  }

  static GetSolsys(galaxy: number, sol: number): Array<Planet>
  {
    let g = Planet.GetAll()[galaxy];
    if(!g) return [];
    let s = g[sol];
    if(!s) return [];
    return s;
  }

  static GetPlanet(galaxy: number, sol: number, idx: number): Planet | null
  {
    let g = Planet.GetAll()[galaxy];
    if(!g) return null;
    let s = g[sol];
    if(!s) return null;
    let p = s[idx];
    if(!p) return null;
    return p;
  }

  static GetPlanetsOfPlayer(playerId: number): Array<Planet>
  {
    let planets: Array<Planet> = [];

    Planet.GetAll().forEach(g => {
      g.forEach(sol => {
        sol.forEach(p => {
          if(p.playerId == playerId) planets.push(p);
        })
      })
    })
    return planets;
  }

  static GetAll() {
    return PLANETS;
  }

  static UpdateAll() {
    PLANETS.forEach(g => g.forEach(s => s.forEach(p => p.Update())));
  }


}

export default Planet;
