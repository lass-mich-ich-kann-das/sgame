import Planet from '../Planet';
import Ressources from '../Ressources';
import { IUpgradeReq } from '../IUpgradeReq';



export default interface IBuilding extends IUpgradeReq {
  Generate(planet: Planet, levelOffset: number): Ressources;
}
