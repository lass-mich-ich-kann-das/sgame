class Validator {
  static IsNumber(v: any): boolean {
    return !isNaN(v);
  }

  static IsSet(v: any): boolean {
    return true;
  }

  static IsAny(values: Array<any>): (v: any) => boolean {
    return (v: any) => {
      return values.filter(val => val == v).length > 0;
    };
  }

  static validate(schema: {[param: string]: (p: any) => boolean}, object: any) {
    let OK = true;
    Object.entries(schema).forEach(([key, val]) => {
      if(typeof object[key] === 'undefined' || !val(object[key])){
        OK = false;
      }
    });
    return OK;
  }
}

export default Validator;