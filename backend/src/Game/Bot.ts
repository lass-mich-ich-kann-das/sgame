import * as DEBUG from 'debug';
import Player from './Player';
import Planet from './Planet';
import EBuildingType from './Buildings/EBuildingType';

const LOG = DEBUG.default('API');
const LOGV = LOG.extend('Verbose');

let BOTLIST: Array<Bot> = [];

class Bot extends Player {
  constructor(playerId: number, name: string, planets: Array<Planet>) {
    super(playerId, name, planets);

    BOTLIST.push(this);
  }

  Update() {
    let productionbuildings = [
        EBuildingType.MineMetal,
        EBuildingType.MineCrystal,
//        EBuildingType.MineDeut,
        EBuildingType.PowerPlant
      ];
    this.planets.forEach(p => {
      let lowestLevel = {
        bulding: EBuildingType.MineMetal,
        level: 1000
      };
      productionbuildings.forEach(e => {
        if(lowestLevel.level > p.buildings.buildingsLvl[e]) {
          lowestLevel.level = p.buildings.buildingsLvl[e];
          lowestLevel.bulding = <EBuildingType>e;
        }
      });
      if(p.buildings.CanUpgrade(lowestLevel.bulding)) {
        LOGV(`Bot ${this.name}(${this.playerId}) Ressources: ${JSON.stringify(p.ressources.JSON())}`);
        LOGV(`Bot ${this.name}(${this.playerId}) Upgrades Building '${lowestLevel.bulding}' to ${lowestLevel.level+1}`);
        p.buildings.Upgrade(lowestLevel.bulding);
      }
    });
  }

  static Start() {
    setInterval(() => {
      BOTLIST.forEach(b => b.Update());
    }, 10000);  
  }
}

export default Bot;

