import IShip from './IShip';
import ShipFighter from './ShipFighter';
import SolarSat from './SolarSat';

export enum EShips {
  Fighter = 'Fighter',
  SolarSat = 'SolarSat'
};

const ShipTypes: {[param: string]: IShip} = {
  [EShips.Fighter]: new ShipFighter(),
  [EShips.SolarSat]: new SolarSat()
};

export default ShipTypes;

