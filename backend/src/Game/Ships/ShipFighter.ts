import IShip, {ShipStats, EShipStats} from './IShip'
import {ResTypes} from '../Ressources'
import EBuildingType from '../Buildings/EBuildingType';
import { IUpgradeReqType } from "../API";

export default class ShipFighter implements IShip {
  stats: ShipStats;
  
  constructor() {
    this.stats = new ShipStats({
      [EShipStats.Armor]: 1000,
      [EShipStats.Shield]: 10,
      [EShipStats.Damage]: 100
    });
  }

  UpgradeReqs(level:number = 0): IUpgradeReqType {
    return {
      ressources: {
        [ResTypes.metal]: 30,
        [ResTypes.crystal]: 10,
      }, buildings: {
        [EBuildingType.Shipyard]: 2
      }
    };
  }

  GetName(): string {
    return 'Fighter';
  }

  Stats(): ShipStats {
    return this.stats;
  }
  
}