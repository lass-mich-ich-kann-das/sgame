import express = require('express');


export default function AddUrlParseTests(router: express.Router) {
  router.get('/test', (req: any, res: any, next: any) => {
    res.json({test: `Hello!`});
  });

  // named param with regex
  router.get('/test/:a(\\d+)', (req: express.Request, res: any, next: any) => {
    res.json({
      url: `/test/a`,
      a: req.params.a
    });
  });

  router.get('/test/:a(\\d+)/:b(\\d+)', (req: express.Request, res: any, next: any) => {
    res.json({
      url: `/test/a/b`,
      a: req.params.a,
      b: req.params.b
    });
  });

  router.get('/test/:a(\\d+)/:word(\\w+)', (req: express.Request, res: any, next: any) => {
    res.json({
      url: `/test/a/word`,
      a: req.params.a,
      word: req.params.word
    });
  });
}
