# General
- Usermanagement
  - login
    - username & password
    - persistent session
  - only owning player is able to control planets/fleets/research


# Objects
  - Galaxy ✅
    - solsystem ✅
      - Planet ✅
  - Ressources ✅
    - metal ✅
    - crystal ✅
    - deuterium ✅
  - Research
    - unlock buildings
    - unlock further research
    - improove ressource generation
    - improove Ships
    - improove defenses
  - Planets
    - store ressources
      - limit ressource generation to a max
    - Buildings
      - Ressource generation
      - unlock further buildings ✅
      - unlock further research
      - require ressources to build / upgrade ✅
    - build ships
      - build ships faster depending on research & buildings
      - require ressources to build
  - Ships
    - do damage
    - have shields
    - have hull
    - can have (dis-)advantage against other ship types
    - require reseach or building to be build
    - move in fleets ✅
  - Fleets
    - consist of ships ✅
    - move between planets ✅
    - can
      - fight
      - transport ressources
      - move/deploy
      - spy
    - take time to move between planets ✅



# Relations
- Player
  - own multiple planets
  - own multiple fleets
    - fleets on one planet are joined to a single fleet
    - sending ships to another planet creates new fleet
  - same research for all planets / fleets
- Galaxy
  - consists of solsystems
    - consists of planets
- Planets
  - max one building by type uprade only
- Fleets
  - any ship type and amount
  - one source
  - one target
  - one type of movement
- research
  - Any type only once upgrade only


# Frontend
- Planets
  - Overview of Planets
    - Ressources, Ships
  - Detailed view of single planet
    - buildings ✅
      - option to build / upgrade ✅
        - show cost
      - description of building and effect (of upgrade)
    - researches
      - option to (further) research
        - show cost
      - description of research and effect
        - show cost
- Fleets
  - overview of all moving fleets
  - create new fleet
    - source and destination planet
    - ship types and amount
    - select movetype/mission
  - recall fleet




# Backend
- REST API returning JSON
  - fleets 
  - planets
    - buildings
  - galaxy overview
  - login
    - validate
    - create auth token ✅
- Update
  - Ressource generation ✅
  - ship building
  - building of buildings
  - research progress
  - fleet movement ✅
    - execute mission action
- persistance
  - load
  - periodically store



✅
❌
