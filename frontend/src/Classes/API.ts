

export interface IRessources {
  energyGenerated: number,
  energyConsumed: number,
  metal: number,
  crystal: number,
  deuterium: number
};


export interface IPosition {
  galaxy: number,
  solar: number,
  idx: number
};


export enum EDirection {
  TO_HOME, TO_TARGET
};

export interface IFleet {
  home: IPosition,
  timeOfArrival: number,
  direction: EDirection,
  target?: IPosition,
  ships: {[param: string]: number}
};

export interface IUpgradeReqType  
{
  ressources?: {[param: string]: number} | undefined, 
  buildings?: {[param: string]: number}, 
};

export interface IBuildingStats {
  lvl: number, 
  resUpgrade: IUpgradeReqType,
  canUpgrade: boolean
};

export interface IPlanet {
  position: IPosition,
  playerId: number,
  name: string,
  ressources: IRessources,
  buildings: IBuildingStats,
  resGeneration: IRessources
};
