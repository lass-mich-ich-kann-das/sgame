import axios from 'axios';
import { userData } from './User';

axios.defaults.baseURL = 'http://localhost:3310';

class Com
{
  static Get(url) {
    console.log(`Bearer ${userData.GetToken()}`);
    return axios.get(url, {
      headers: { Authorization: `Bearer ${userData.GetToken()}` }
    });
  }

  static Post(url, param) {
    console.log(`Bearer ${userData.GetToken()}`);
    return axios.post(url, param, {
      headers: { Authorization: `Bearer ${userData.GetToken()}` }
    });
  }
}

export default Com;
