import * as event from 'events';
import Com from './Com';
import { IRessources, IPlanet, IFleet, IPosition } from './API';

class UserData
{
  planetsUpdated: number;
  token: string | null;
  name: string;
  event: event.EventEmitter;
  planets: Array<IPlanet>;
  planetSelected: number;

  fleetsUpdated: number;
  fleets: Array<IFleet>;
    
  
  constructor()
  {
    this.token = localStorage.getItem('token');
    this.name = 'unknown';
    this.event = new event.EventEmitter();
    this.event.on('update', token => {
      console.log('Updated ' + token);
    });
    this.event.emit('update');
    this.planets = [];
    this.planetsUpdated = Date.now();
    this.planetSelected = 0;

    this.fleetsUpdated = Date.now();
    this.fleets = [];

    this.event.on('selectPlanet', () => {
      console.log('selectPlanet', this.GetPlanetSelected()?.name);
    });
  }

  /**
   * Gets UserName
   */
  GetName() {
    return this.name;
  }

  /**
   * Gets Userdata from Token
   */
  GetData(){
    if(!this.token) return;
    var base64Url = this.token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  UpdateFromToken(token: string){
    this.event.emit('update', token);
    localStorage.setItem('token', token);
    this.token = token;
  }

  GetToken() {
    return this.token;
  }




  UpdateFleets() {
    Com.Get(`/api/fleet/list`).then((res: any) => {
      this.SetFleets(res.data);
    }).catch();
  }

  SetFleets(fleets: Array<IFleet>) {
    this.fleetsUpdated = Date.now();
    this.fleets = fleets;
    this.event.emit('updateFleets');
  }


  /**
   * Sends a fleet to position
   * 
   * @param reason either attack|hold|transport
   * @param fromPos Position of start planet
   * @param toPos Position of target planet
   * @param ships ships and amount to send 
   */
  SendFleet(reason: string, fromPos: IPosition, toPos: IPosition, ships: {[param: string]: number}) {
    return Com.Post(`/api/fleet/${reason}/${fromPos.galaxy}/${fromPos.solar}/${fromPos.idx}/${toPos.galaxy}/${toPos.solar}/${toPos.idx}`, ships).then((res: any) => {
      return res;
    });
  }



  SetPlanets(planets: Array<IPlanet>) {
    this.planetsUpdated = Date.now();
    this.planets = planets;
    this.event.emit('selectPlanet');
    this.event.emit('update');
  }

  UpdatePlanets() {
    Com.Get(`/api/planet/my`).then((res: any) => {
      this.SetPlanets(res.data);
    }).catch();
  }

  GetPlanets() {
    return this.planets;
  }

  SelectPlanet(planet: IPlanet) {
    this.planetSelected = this.planets.indexOf(planet);
    this.event.emit('selectPlanet');
  }

  GetPlanetSelected(): IPlanet | null {
    if(this.planetSelected)
      return this.planets[this.planetSelected];
    if(this.planets && this.planets.length > 0)
      return this.planets[0];
    return null;
  }

  /**
   * Estimate Ressources of selected Planet
   */
  EstimateResOfSelPlanet() {
    let gen = {
      metal: 0,
      crystal: 0,
      deuterium: 0
    };
    let res = {
      metal: 0,
      crystal: 0,
      deuterium: 0,
      efficiency: 0,
    };

    let planet = userData.GetPlanetSelected();
    if(planet && planet.ressources && planet.resGeneration) {
      let diff = Date.now() - userData.planetsUpdated;
      diff /= 1000;

      let resLastUpdate = planet.ressources;
      let gen = planet.resGeneration;
      res = {
        metal:      +(resLastUpdate.metal + diff * gen.metal).toFixed(2),
        crystal:    +(resLastUpdate.crystal + diff * gen.crystal).toFixed(2),
        deuterium:  +(resLastUpdate.deuterium + diff * gen.deuterium).toFixed(2),
        efficiency: Math.min(1, resLastUpdate.energyGenerated / resLastUpdate.energyConsumed)
      } 
    }

    return {gen, res};
  }
};

const userData = new UserData();

export { userData };
export default UserData;
