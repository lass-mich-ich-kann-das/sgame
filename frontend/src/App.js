import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import UserContext from './Contexts/User';
import { userData } from './Classes/User';
import Login from './Components/Login';
import Navigation from './Components/Navigation';
import Galaxy from './Components/Galaxy';
import PlanetList from './Components/PlanetList';
import PlanetBuildings from './Components/Planet/Buildings'
import Account from './Components/Account';
import Ressources from './Components/Ressources';
import FleetList from './Components/FleetList';





class App extends React.Component {
  state = {
    user: userData
  }

  componentDidMount() {
    userData.event.on('update', () =>{
      this.setState({});
    });
    userData.UpdatePlanets();
  }

  render()
  {
    return (
      <HashRouter>
        <UserContext.Provider value={this.state.user}>
          <Navigation />
          <Ressources />
          <Switch>
            <Route exact path='/login'>
              <Login/>
            </Route>
            <Route exact path='/account'>
              <Account/>
            </Route>
            <Route exact path='/galaxy'>
              <Galaxy/>
            </Route>
            <Route exact path='/Planet'>
              <PlanetBuildings/>
            </Route>
            <Route exact path='/FleetList'>
              <FleetList/>
            </Route>
          </Switch>
          <PlanetList />
        </UserContext.Provider>
      </HashRouter>
    );
  }
}

export default App;
