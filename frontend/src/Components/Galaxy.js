import React from 'react';
import Com from '../Classes/Com';


class SolsysPlanet extends React.Component {
  render() {
    let {planet} = this.props;
    if(!planet){
      return(<tr>
        <td>-:-:-</td>
        <td>-</td>
        <td>-</td>
      </tr>);
    }

    let pos = planet.position;
    return(<tr>
      <td>{pos.galaxy}:{pos.solar}:{pos.idx}</td>
      <td>{planet.playerId}</td>
      <td>{planet.name}</td>
    </tr>);
  }
}

class Galaxy extends React.Component {
  state = {
    content: [],
    pos: {
      galaxy: 0,
      sol: 0
    }
  }
  
  constructor(props) {
    super(props);
    this.Update = this.Update.bind(this);
    this.SolNext = this.SolNext.bind(this);
    this.SolPrev = this.SolPrev.bind(this);
    this.GalaxyNext = this.GalaxyNext.bind(this);
    this.GalaxyPrev = this.GalaxyPrev.bind(this);
  }

  componentDidMount() {
    this.Update(0, 0);
  }

  Update(galaxy, sol){

    Com.Get(`/api/galaxy/${galaxy}/${sol}`).then(result => {
      console.debug(result.data);
      this.setState({
        content: result.data,
        pos: {galaxy, sol}
      });
    });
  }
  
  SolNext(e) {
    this.Update(this.state.pos.galaxy, this.state.pos.sol +1);
    e.preventDefault();
  }
  
  SolPrev(e) {
    this.Update(this.state.pos.galaxy, this.state.pos.sol -1);
    e.preventDefault();
  }
  
  GalaxyNext(e) {
    this.Update(this.state.pos.galaxy + 1, this.state.pos.sol);
    e.preventDefault();
  }
  
  GalaxyPrev(e) {
    this.Update(this.state.pos.galaxy -1, this.state.pos.sol);
    e.preventDefault();
  }

  render() {
    let {galaxy, sol} = this.state.pos;
    return (
      <div className="content">
        Solarsystem {galaxy}:{sol}<br />
        <a href="#" onClick={this.GalaxyPrev}>&lt;&lt;</a>&nbsp;&nbsp;
        <a href="#" onClick={this.SolPrev}>&lt;</a>&nbsp;||&nbsp;
        <a href="#" onClick={this.SolNext}>&gt;</a>&nbsp;
        <a href="#" onClick={this.GalaxyNext}>&gt;&gt;</a>
        <hr />
        <table>
          <tbody>
            <tr>
              <td>Pos</td>
              <td>playerId</td>
              <td>name</td>
            </tr>
            {this.state.content.map((p, idx) => <SolsysPlanet key={idx} planet={p}/>)}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Galaxy;
