import React from 'react';
import { userData } from '../Classes/User';
import Com from '../Classes/Com';


class PlanetListE extends React.Component {
  render() {
    let {planet} = this.props;
    let pos = planet.position;

    return(
      <li>
        <a href="#" onClick={(e)=> {e.preventDefault(); userData.SelectPlanet(planet);}}>
          [{pos.galaxy}:{pos.solar}:{pos.idx}]
          {planet.name} 
          </a>
      </li>
    );
  }
}

class PlanetList extends React.Component {
  render() {
    let planets = userData.GetPlanets();
    
    console.debug({planets});
    if(!planets){
      planets = [];
    }

    return(
      <div className="nav-right">
        <ul>
          {planets.map((p, i) => <PlanetListE key={i} planet={p} />)}
        </ul>
      </div>
    );
  }
}

export default PlanetList;
