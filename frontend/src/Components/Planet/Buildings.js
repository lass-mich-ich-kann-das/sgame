import React from 'react';
import { userData } from '../../Classes/User';
import Com from '../../Classes/Com';


class Building extends React.Component {  
  state = {
    canUpgrade: false,
    upgradeReqs: {}
  };

  constructor(props) {
    super(props);
    
    this.Update = this.Update.bind(this);
  }

  componentDidMount() {
    this.Update();
    this.interval = setInterval(this.Update, 1000);
  }

  componentWillUnmount() {
    if(this.interval) clearInterval(this.interval);
  }

  Update() {
    let canUpgrade = true;
    let planet = userData.GetPlanetSelected();
    console.log({type: this.props.type, planet});
    let {resUpgrade} = planet.buildings[this.props.type];
    console.log(resUpgrade);
    if(resUpgrade){
      if(canUpgrade && resUpgrade.ressources) {
        Object.entries(resUpgrade.ressources)
            .forEach(([r, amount]) => {
          if(planet.ressources[r] < amount)
            canUpgrade = false;
        });
      }
      if(canUpgrade && resUpgrade.buildings) {
        Object.entries(resUpgrade.buildings)
            .forEach(([b, level]) => {
          console.log([b, level], planet.buildings[b]);
          if(planet.buildings[b].lvl < level)
            canUpgrade = false;
        });
      }
      this.setState({
        canUpgrade, 
        upgradeReqs: resUpgrade
      });
    }
  }

  render() {
    let building = this.props;
    let style = {color: this.state.canUpgrade?'green':'red'};
    return(
      <div>
        <p style={style}>
          {building.type}({building.lvl})
          {this.state.canUpgrade ? (
            <a href='#' onClick={(e) => {e.preventDefault(); this.props.Upgrade();}}>Upgrade</a>
          ) : true}
        </p>
        {JSON.stringify(this.state.upgradeReqs)}
      </div>
    );
  }
}

class PlanetBuildings extends React.Component {
  state = {
  }

  constructor(props) {
    super(props);
    
    this.Update = this.Update.bind(this);
  }

  componentDidMount() {
    userData.event.on('selectPlanet', this.Update);
    userData.event.on('update', this.Update);
    this.Update();
  }

  componentWillUnmount() {
    userData.event.removeListener('selectPlanet', this.Update);
    userData.event.removeListener('update', this.Update);
  }

  Update() {
    console.log(`Updating PlanetBuilding`);
    this.setState({
      planet: userData.GetPlanetSelected()
    });
  }

  Upgrade(building) {
    let {planet} = this.state;
    if(!planet) return;

    let pos = planet.position;
    Com.Get(`/api/planet/building/up/${pos.galaxy}/${pos.solar}/${pos.idx}/${building}`).then(() => {
      userData.UpdatePlanets();
    });
  }

  render() {
    let buildings = [];
    let {planet} = this.state;
    if(!planet) {
      return(<div>No Planet selected</div>);
    }

    for(let [key, value] of Object.entries(planet.buildings)){
      buildings.push(
        <Building key={key} type={key} lvl={value.lvl} Upgrade={() => this.Upgrade(key)}/>
      )
    }
  
    let pos = planet.position;
    let res = planet.ressources;
    return(
      <div className="content">
        <p>Position: {pos.galaxy}:{pos.solar}:{pos.idx}</p>
        <p>Name: {planet.name}</p>
        <hr />
        {buildings}
      </div>
    );
  }
}

export default PlanetBuildings;
