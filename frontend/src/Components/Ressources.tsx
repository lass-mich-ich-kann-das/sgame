import React, { Props } from 'react';
import { userData } from '../Classes/User';


export interface IState {
  gen: {
    metal:     number;
    crystal:   number;
    deuterium: number;
  };
  res: {
    metal:      number;
    crystal:    number;
    deuterium:       number;
    efficiency: number;  
  };
}


class Ressources extends React.Component<any, IState>  {
  state: IState = {
    gen: {
      metal: 0,
      crystal: 0,
      deuterium: 0
    },
    res: {
      metal: 0,
      crystal: 0,
      deuterium: 0,
      efficiency: 0
    }
  }
  interval: NodeJS.Timeout | null;

  constructor(props: any) {
    super(props);
    this.interval = null;
    this.Update = this.Update.bind(this);    
  }

  componentDidMount() {
    this.interval = setInterval(() => this.Update(), 1000);
  }

  componentWillUnmount() {
    if(this.interval) clearInterval(this.interval);
  }

  Update() {
    this.setState(userData.EstimateResOfSelPlanet());
  }

  render() {
    return(
      <div className="ressources">
        <div>Metal: +{this.state.gen.metal.toFixed(2)}/s<br />{this.state.res.metal}</div>
        <div>Crystal: +{this.state.gen.crystal.toFixed(2)}/s<br />{this.state.res.crystal}</div>
        <div>Deut: +{this.state.gen.deuterium.toFixed(2)}/s<br />{this.state.res.deuterium}</div>
        <div><br />Efficiency:{this.state.res.efficiency.toFixed(2)}</div>

      </div>
    );
  }
}

export default Ressources;
