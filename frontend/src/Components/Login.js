import React from 'react';
import { Redirect } from 'react-router-dom';
import Com from '../Classes/Com';

import UserContext from '../Contexts/User';

class Login extends React.Component {
  static contextType = UserContext;
  state = {
    user: '', pass: ''
  }

  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.name === 'isGoing' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      loggedIn: false,
      [name]: value
    });
  }

  handleSubmit(event) {
//    alert('An essay was submitted: ' + JSON.stringify(this.state));
    event.preventDefault();
    Com.Post('/login', {
      user: this.state.user,
      pass: this.state.pass
    }).then(result => {
      if(!result) return;

      console.log(result);
      if(result.data && result.data.login){
        this.context.UpdateFromToken(result.data.token);
        this.setState({loggedIn: true});
      }
    });
  }

  render() {
    if(this.state.loggedIn){
      return(<Redirect to="/" />);
    }
    return (
      <div className="login">
        Login
        
        <form onSubmit={this.handleSubmit}>
        <input name="user"
            onChange={this.handleInputChange} />
          <input name="pass" type="password"
            onChange={this.handleInputChange} />
          <input type="submit" value="Submit"/>
        </form>
      </div>
    );
  }
}

export default Login;
