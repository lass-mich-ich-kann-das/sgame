import React from 'react';
import Com from "../Classes/Com";


class Fleet extends React.Component {

  render() {
    let ships = [];
    Object.entries(this.props.fleet.ships)
        .forEach(([s, amount]) => {
          ships.push(<li>{s}: {amount}</li>)
    });

    return (
      <div>
        {JSON.stringify(this.props.fleet)}
        <ul>
          {ships}
        </ul>
      </div>
    );
  }
}

class Fleetlist extends React.Component {
  state = {
    fleets: []
  }

  componentDidMount() {
    Com.Get(`/api/fleet/list`).then(res => {
      this.setState({fleets: res.data});
    });
  }
  
  render() {
    return(
      <div className="content">
        {this.state.fleets.map((f, idx) => (<Fleet key={idx} fleet={f}/>))}
      </div>
    );
  }
}

export default Fleetlist;
